module ApplicationHelper
  #Return Title for page
  def page_title(page_title = "")
    base_title = "Track Your Time"
    if page_title.empty?
      base_title
    else
      base_title + " | " + page_title
    end
  end
end
