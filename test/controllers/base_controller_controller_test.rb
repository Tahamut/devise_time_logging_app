require 'test_helper'

class BaseControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get base_controller_home_url
    assert_response :success
  end

  test "should get help" do
    get base_controller_help_url
    assert_response :success
  end

  test "should get about" do
    get base_controller_about_url
    assert_response :success
  end

end
