Rails.application.routes.draw do
  get '/home', to: 'base_controller#home'

  get '/help', to: 'base_controller#help'

  get '/about', to: 'base_controller#about'

  root 'base_controller#home'
end
